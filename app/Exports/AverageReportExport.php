<?php

namespace App\Exports;

use App\Invoice;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithColumnWidths;

class AverageReportExport implements
    ShouldAutoSize,
    FromArray,
    WithTitle,
    WithEvents,
    WithColumnWidths
{
    protected $invoices;
    protected $sheet_title;

    public function __construct(array $invoices, $sheet_title, $start_date, $end_date)
    {
        $this->invoices = $invoices;
        $this->sheet_title = $sheet_title;
        $this->start_date = $start_date;
        $this->end_date = $end_date;
    }

    public function title(): string
    {
        return $this->sheet_title;
    }

    public function columnWidths(): array
    {
        if ($this->sheet_title === 'CA-Respuestas') {
            return [
                'N' => 85,           
            ];
        }
        else return [];
    }

    public function array(): array
    {
        $start = $this->start_date ? $this->start_date : 'Todos';
        $end = $this->end_date ? $this->end_date : 'Todos';

        $filters_rows = [
            ['Filtrado desde: ', $start],
            ['Filtrado hasta: ', $end],
            [" "]
        ];

        $all_rows = array_merge($filters_rows, $this->invoices);

        return $all_rows;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                // Headers en negrita
                $event->sheet->getDelegate()->getStyle('A4:Z4')->getFont()->setBold(true);

                if ($this->sheet_title === 'CA-Respuestas') {
                    $event->sheet->getDelegate()->getStyle('N')->getAlignment()->setWrapText(true);
                }
            },
        ];
    }

}
