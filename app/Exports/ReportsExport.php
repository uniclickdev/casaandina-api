<?php

namespace App\Exports;

use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ReportsExport implements WithMultipleSheets
{
    use Exportable;

    protected $start_date;
    protected $end_date;
    protected $average_compliance_by_region;
    protected $users_complete;
    protected $users_incomplete;
    protected $respuestas;

    public function __construct(
            $start_date,
            $end_date,
            $average_compliance_by_region,
            $users_complete,
            $users_incomplete,
            $respuestas
        )
    {
        $this->start_date = $start_date;
        $this->end_date = $end_date;
        $this->average_compliance_by_region = $average_compliance_by_region;
        $this->users_complete = $users_complete;
        $this->users_incomplete = $users_incomplete;
        $this->respuestas = $respuestas;
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];
        $sheets[] = new AverageReportExport($this->average_compliance_by_region, 'Prom. Rptas por región', $this->start_date, $this->end_date);
        $sheets[] = new AverageReportExport($this->users_incomplete, 'Encuestas incompletas', $this->start_date, $this->end_date);
        $sheets[] = new AverageReportExport($this->users_complete, 'Encuestas completas', $this->start_date, $this->end_date);
        $sheets[] = new AverageReportExport($this->respuestas, 'CA-Respuestas', $this->start_date, $this->end_date);

        return $sheets;
    }
}
