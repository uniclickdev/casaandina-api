<?php

namespace App\Imports;

use App\Models\User;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;
HeadingRowFormatter::default('none');
use DB;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class UserImport implements ToCollection, WithHeadingRow, WithMultipleSheets 
{
    public function sheets(): array
    {
        return [
            new FirstSheetImport()
        ];
    }

    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) 
        {
            if($row['Nombre']) {
                //$position = DB::table('positions')->select('*')->where('name',$row['Area'])->first();
                //$hotel = DB::table('hotels')->select('*')->where('name',$row['Hotel'])->first();

                $userExist = DB::table('users')
                    ->select('*')
                    ->where('email', $row['Correo'])
                    ->first();

                if($userExist && ($row['reingreso'] == 'si' || $row['reingreso'] == 'sí' )) {
                    DB::table('users')
                        ->select('*')
                        ->where('id', $userExist->id)
                        ->update([
                            'name' => $row['Nombre'],
                            'lastname' => $row['Apellido'],
                            'document' => $row['DNI'],
                            'area_admin' => $row['Area'],
                            'hotel_admin' => $row['Hotel'],
                            're_entry' => $row['reingreso'],
                            'is_pdf' => 0,
                            'created_at' => now()
                        ]);
                }
                else {
                    DB::beginTransaction();

                    [$identificator, $secret_password] = app('App\Http\Controllers\API\V1\User\UserController')->generate_secret_password();
                    $secret_password = $secret_password . $identificator;
                    $user = User::create([
                        'name' => $row['Nombre'],
                        'lastname' => $row['Apellido'],
                        'document' => $row['DNI'],
                        'area_admin' => $row['Area'],
                        'hotel_admin' => $row['Hotel'],
                        'email' => $row['Correo'],
                        'identificator' => strtolower($identificator),
                        'password' => bcrypt(strtolower($secret_password)),
                        'aux_password' => strtolower($secret_password),
                        're_entry' => $row['reingreso'],
                        'is_pdf' => 0,
                        'created_at' => now()
                    ]);

                    $user->assignRole('worker');
                    $user->suitcase()->create(['name' => 'mySuitCase']);

                    DB::commit();
                }
            }
        }
    }
}
