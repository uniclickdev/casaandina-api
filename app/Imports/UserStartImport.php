<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class UserStartImport implements WithMultipleSheets
{
    public function sheets(): array
    {
        return [
            'Hoja1' => new UserImport(),
        ];
    }
}