<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class HotelResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->city->name,
            'description' => $this->description,
            'region' => $this->city->region->name,
            'clasification' => $this->service,
            // 'city' => $this->city->name,
            'departament' => $this->city->departament,
            'isActive' => !($this->deleted_at),
            'name_aux' => $this->name,
        ];
    }
}
