<?php

namespace App\Http\Resources;

use App\Models\Position;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' =>  $this->name,
            'lastname' =>  $this->lastname,
            'email' =>  $this->email,
            'document' => $this->document,
            'gender' =>  $this->gender,
            'hotel' =>  new HotelResource($this->worker_hotel),
            'position' =>  new PositionResource($this->position),
            'strength' =>  new StrengthResource($this->strength),
            'experience' => $this->experience,
            'date_start_game' => $this->date_start_game,
            'date_end_game' => $this->date_end_game,
            'compromise' => $this->compromise,
        ];
    }
}
