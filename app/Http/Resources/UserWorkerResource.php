<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use DB;

class UserWorkerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'date_start_game' => $this->date_start_game,
            'date_end_game' => $this->date_end_game,
            'name' => $this->name,
            'lastname' => $this->lastname,
            'email' => $this->email,
            'nroDocument' => $this->document,
            'areaAdmin' => $this->area_admin,
            'hotelAdmin' => $this->hotel_admin,
            'hotel' =>  $this->position_id == 2 || $this->position_id == 3 ? null : new HotelResource($this->worker_hotel),
            'position' =>  new PositionResource($this->position),
            'strength' =>  new StrengthResource($this->strength),
            'compromise' => $this->compromise,
            'score_total' => $this->experience,
            'progress' => round(DB::table('hotel_user')->where('user_id',$this->id)->count() / 10 , 2) * 100,
            'last_notification' => $this->last_notification,
            're_entry' => $this->re_entry,
        ];
    }
}
