<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|string|max:150',
            'lastname'=>'required|string|max:150',
            'gender'=>'required|string|max:50',
            'hotel'=>'required|string|max:150',
            'position'=>'required|string|max:100',
            'strength'=>'required|string|max:100',
            'compromise'=>'nullable|string',
            'score_total'=>'nullable|numeric',
            'email'=>'required|email|unique:users',
            'password'=>'required|min:8'
        ];
    }
}
