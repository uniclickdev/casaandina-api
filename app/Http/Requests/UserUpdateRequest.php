<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'nullable',
            'lastname' => 'nullable',
            'gender' => 'nullable',
            'document'=>'nullable|numeric',

            'date_start_game' => 'nullable|date_format:Y-m-d H:i:s',
            'date_end_game' => 'nullable|date_format:Y-m-d H:i:s',

            'hotel' => 'nullable',
            'hotel.id' => 'required_with:hotel|numeric|exists:hotels,id',

            'position' => 'nullable',
            'position.id' => 'required_with:position|numeric|exists:positions,id',

            'strength' => 'nullable',
            'strength.id' => 'required_with:strength|numeric|exists:strengths,id',

            'experience' => 'nullable|numeric',
            'compromise' => 'nullable|string'
        ];
    }
}
