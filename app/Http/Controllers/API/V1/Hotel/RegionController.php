<?php

namespace App\Http\Controllers\API\V1\Hotel;

use App\Http\Controllers\Controller;
use App\Http\Requests\Hotel\RegionStoreRequest;
use App\Http\Requests\Hotel\RegionUpdateRequest;
use App\Http\Resources\RegionHotelResource;
use App\Http\Resources\RegionResource;
use App\Models\Hotel;
use App\Models\Region;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Throwable;

class RegionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $regions = Region::all();
        return RegionResource::collection($regions);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RegionStoreRequest $request)
    {
        $region = Region::create($request->all());
        return (new RegionResource($region))->additional(['message' => 'Region Created']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Region  $region
     * @return \Illuminate\Http\Response
     */
    public function show(Region $region)
    {
        return new RegionResource($region);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Region  $region
     * @return \Illuminate\Http\Response
     */
    public function update(RegionUpdateRequest $request, Region $region)
    {
        $region->update($request->all());
        return (new RegionResource($region))->additional(['message' => 'Region Updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Region  $region
     * @return \Illuminate\Http\Response
     */
    public function destroy(Region $region)
    {
        $region->delete();
        return (new RegionResource($region))->additional(['message' => 'Region Destroy']);
    }
    public function index_region_hotels(Region $region)
    {
        $hotels = collect();
        $cities = $region->cities;
        $cities->map(function ($city) use (&$hotels) {
            $hotels = $hotels->merge($city->hotels);
        });
        return RegionHotelResource::collection($hotels);
    }
    public function show_region_hotels(Region $region, Hotel $hotel)
    {
        $hotels = $this->index_region_hotels($region);
        $hotelBelongToRegion = $hotels->contains('id', $hotel->id);
        if (!$hotelBelongToRegion) throw new ModelNotFoundException();
        return new RegionHotelResource($hotel);
    }
}
