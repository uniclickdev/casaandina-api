<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\LoginSecretPasswordRequest;
use App\Http\Requests\User\UserStoreRequest;
use App\Http\Resources\UserWorkerResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    public function login(LoginRequest $request)
    {
        if (!Auth::attempt($request->only('email', 'password'))) {
            throw ValidationException::withMessages(['message' => ['Credentials invalid']]);
        }
        $token = $request->user()->createToken($request->email)->plainTextToken;
        return response()->json([
            'access_token' => $token,
            'type_token' => 'Bearer'
        ], 200);
    }
    public function login_secret_password(LoginSecretPasswordRequest $request)
    {
        // Composition Secret Password (length = 5)
        // where 2 final digits are identificator

        // convert to lowercase because data password was insert in lowercase
        $request_password = strtolower($request->secret_password);

        $identificator = substr($request_password, -2);
        $password = $request_password;

        $user = User::where('identificator', $identificator)->first();
        // dd(strtolower($password), strtolower($user->password));
        if (!$user || !Hash::check($password, $user->password)) {
            if(!$user->password_2) {
                throw ValidationException::withMessages(['message' => ['Credentials invalid']]);
            } else {
                if (!Hash::check($password, $user->password_2)) {
                    throw ValidationException::withMessages(['message' => ['Credentials invalid']]);
                }
            }
            
        }

        $token = $user->createToken($request->secret_password)->plainTextToken;
        return response()->json([
            'user_id' => $user->id,
            'access_token' => $token,
            'type_token' => 'Bearer',
            'data' => new UserWorkerResource($user)
        ], 200);
    }
    public function logout()
    {
        auth()->user()->tokens()->delete();
        return response()->json([
            'message' => 'You have successfully logged out'
        ], 200);
    }
    public function register(UserStoreRequest $request)
    {
        $user = User::create($request->all());
        $user->password = bcrypt($request->password);
        $user->save();
        $token = $user->createToken($user->email)->plainTextToken;
        return response()->json([
            'access_token' => $token,
            'type_token' => 'Bearer'
        ], 201);
    }
}
