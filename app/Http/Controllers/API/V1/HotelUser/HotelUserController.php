<?php

namespace App\Http\Controllers\API\V1\HotelUser;

use App\Http\Controllers\Controller;
use App\Http\Requests\HotelUsuario\HotelUserStoreRequest;
use App\Http\Resources\HotelUserResource;
use App\Models\Hotel;
use App\Models\User;
use Illuminate\Http\Request;

class HotelUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $this->authorize('Yourself', $user);
        return HotelUserResource::collection($user->hotels)->additional([
            'score_total' => $user->hotels->sum('pivot.score'),
            'coins_total' => $user->hotels->sum('pivot.coins'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HotelUserStoreRequest $request, User $user, Hotel $hotel)
    {
        //     $user->hotels()->attach($hotel->id,[
        //         'score'=>$request->score,
        //         'coins'=>$request->coins
        //     ]);
        //     return (new HotelUserResource($user->hotels()->where('hotel_id',$hotel->id)->firstOrFail()))->additional(['message' => 'Hotel User Created']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user, Hotel $hotel)
    {
        $this->authorize('Yourself', $user);
        return new HotelUserResource($user->hotels()->where('hotel_id', $hotel->id)->firstOrFail());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(HotelUserStoreRequest $request, User $user, Hotel $hotel)
    {
        $this->authorize('Yourself', $user);
        // If not exists resource, then created and before update fields
        if (!$user->hotels()->where('hotel_id', $hotel->id)->first()) {
            $user->hotels()->attach($hotel->id);
        }
        $user->hotels()->updateExistingPivot($hotel->id, [
            'score' => $request->score,
            'coins' => $request->coins
        ]);
        $HotelUser = $user->hotels()->where('hotel_id', $hotel->id)->firstOrFail();
        return (new HotelUserResource($HotelUser))->additional(['message' => 'Hotel User Created']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
