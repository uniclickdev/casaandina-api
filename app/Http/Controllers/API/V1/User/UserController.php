<?php

namespace App\Http\Controllers\API\V1\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\SecretPasswordRequest;
use App\Http\Requests\User\UserUpdateCompromiseRequest;
use App\Http\Requests\User\UserUpdateScoreTotalRequest;
use App\Http\Requests\UserSuitcaseUpdateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Resources\SecretPasswordResource;
use App\Http\Resources\UserResource;
use App\Http\Resources\UserSuitcaseResource;
use App\Http\Resources\UserWorkerResource;
use App\Imports\UserImport;
use App\Imports\UserStartImport;
use App\Models\User;
use Error;
use Excel;
use Exception;
use Illuminate\Auth\Access\Gate;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Throwable;
use PDF;
use Storage;
use LynX39\LaraPdfMerger\Facades\PdfMerger;
use File;
use App\Mail\SendNotificationMail;
use Mail;

class UserController extends Controller
{
    public function index()
    {
        return UserResource::collection(User::all());
    }

    //Tabla admin
    //Si lo modificas, modifica el excel respuestas()
    public function index_workers(Request $request)
    {
        $request->validate([
            'start_date' => 'nullable|date',
            'end_date' => 'nullable|date',
            'name' => 'nullable|string'
        ]);
        
        $start_date = $request->has('start_date') ? $request->start_date : null;
        $end_date = $request->has('end_date') ? $request->end_date : null;
        $name = $request->has('name') ? $request->name : null;

        $workers = User::role('worker')
            ->when((!empty($start_date) && !empty($end_date)), function ($query) use ($start_date, $end_date) {
                $query->whereDate('users.created_at', '>=', $start_date)
                    ->whereDate('users.created_at', '<=', $end_date);
            })
            ->when((!empty($name)), function ($query) use ($name) {
                $words = explode(' ', $name);
                $query->where(function ($subquery) use ($words) {
                    foreach ($words as $word) {
                        $subquery->orWhereRaw('LOWER(users.name) LIKE ?', ['%' . strtolower($word) . '%'])
                            ->orWhereRaw('LOWER(users.lastname) LIKE ?', ['%' . strtolower($word) . '%']);
                    }
                });
            })
            ->orderBy('id', 'desc')
            ->get();

        return UserWorkerResource::collection($workers);
    }

    public function show_worker(User $user)
    {
        if (!$user->hasRole('worker')) throw new ModelNotFoundException();
        return new UserWorkerResource($user);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $this->authorize('Yourself', $user);
        return new UserResource($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, User $user)
    {
        DB::beginTransaction();
        try {
            $this->authorize('Yourself', $user);

            $hotel = $request->hotel;
            $position = $request->position;
            $strength = $request->strength;
            $user->update($request->except(['hotel', 'position', 'strength']));
            if ($hotel) $user->worker_hotel()->associate($hotel["id"])->save();
            if ($position) $user->position()->associate($position["id"])->save();
            if ($strength) $user->strength()->associate($strength["id"])->save();

            DB::commit();
            return (new UserResource($user))->additional(['message' => 'User Updated']);
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
    public function update_compromise(UserUpdateCompromiseRequest $request, User $user)
    {
        $this->authorize('Yourself', $user);
        $user->update($request->only('compromise'));
        return (new UserResource($user))->additional(['message' => 'Compromise User Updated']);
    }
    public function updateScoreTotal(UserUpdateScoreTotalRequest $request, User $user)
    {
        $user->update($request->only('score_total'));
        return (new UserResource($user))->additional(['message' => 'Score Total User Updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
    public function store_secret_password(SecretPasswordRequest $request)
    {

        try {
            DB::beginTransaction();
            $secret_passwords = collect();
            for ($i = 0; $i < $request->count; $i++) {


                [$identificator, $secret_password] = $this->generate_secret_password();
                $secret_password = $secret_password . $identificator;
                // insert password data in lowercase, 
                // to compare in the future and to be 
                // indifferent to uppercase and lowercase letters
                $user = User::create([
                    'identificator' => strtolower($identificator),
                    'password' => bcrypt(strtolower($secret_password))
                ]);
                $user->assignRole('worker');
                $user->suitcase()->create(['name' => 'mySuitCase']);

                $secret_password = collect(['secret_password' => $secret_password]);
                $secret_passwords->push($secret_password);
            }
            DB::commit();
            return SecretPasswordResource::collection($secret_passwords);
        } catch (Exception $th) {
            DB::rollBack();
            throw new Error($th);
        }
    }
    public function generate_secret_password()
    {
        // Secret Pass Structure [secret(3) + id(2)] = 5 Digitis
        
        do {
            $identificator = Str::random(2);
            $existIdentificator = User::where('identificator', strtolower($identificator))->first();
        } while ($existIdentificator);
        $secret_password = Str::random(5 - strlen($identificator));
        return [$identificator, $secret_password];
    }
    public function show_suitcase(User $user)
    {
        $this->authorize('Yourself', $user);
        if (!$user->suitcase) throw new ModelNotFoundException();
        return new UserSuitcaseResource($user->suitcase);
    }
    public function update_suitcase(UserSuitcaseUpdateRequest $request, User $user)
    {
        try {
            DB::beginTransaction();
            $this->authorize('Yourself', $user);
            $things = [];
            // If not exists resource, then created and before update fields
            if (!$user->suitcase) {
                $suitcase = $user->suitcase()->create(['name' => $request->name]);
                $user->suitcase = $suitcase;
            };

            foreach ($request->things as $value) {
                array_push($things, $value["id"]);
            }
            $user->suitcase->things()->sync($things);
            DB::commit();
            return (new UserSuitcaseResource($user->suitcase))->additional(['message' => 'Suitcase Updated'])->response()->setStatusCode(200);
        } catch (Exception $th) {
            DB::rollBack();
            throw new Error('Something did not go as expected, please try again');
        }
    }

    public function store_secret_password_pdf(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'lastname' => 'required',
            'dni' => 'required',
            'selectArea' => 'required',
            'selectHotel' => 'required',
            'email' => 'required|email',
            'reEntry' => 'required|in:si,no'
        ]);

        try {
            $userExist = User::where('email',$request->email)
                ->orderByDesc('created_at')
                ->first();

            if($userExist) {
                if($userExist->hasRole('admin')) {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Este correo pertenece a un administrador. Por favor, ingresa otro correo.',
                    ], 400);
                }

                if($request->reEntry == 'si' || $request->reEntry == 'sí') {
                    DB::table('users')->where('id',$userExist->id)->update([
                        'name' => $request->name,
                        'lastname' => $request->lastname,
                        'document' => $request->dni,
                        'area_admin' => $request->selectArea,
                        'hotel_admin' => $request->selectHotel,
                        're_entry' => $request->reEntry,
                    ]);
                    $final_password = $userExist->aux_password;
                }
                elseif($request->reEntry == 'no') {
                    DB::beginTransaction();
                    [$identificator, $secret_password] = $this->generate_secret_password();
                    $secret_password = $secret_password . $identificator;
                    $user = User::create([
                        'name' => $request->name,
                        'lastname' => $request->lastname,
                        'document' => $request->dni,
                        'area_admin' => $request->selectArea,
                        'hotel_admin' => $request->selectHotel,
                        'email' => $request->email,
                        'identificator' => strtolower($identificator),
                        'password' => bcrypt(strtolower($secret_password)),
                        'aux_password' => $secret_password,
                        're_entry' => $request->reEntry,
                    ]);
                    $user->assignRole('worker');
                    $user->suitcase()->create(['name' => 'mySuitCase']);
                    DB::commit();
                    $final_password = $secret_password;
                }
            }
            else {
                DB::beginTransaction();
                [$identificator, $secret_password] = $this->generate_secret_password();
                $secret_password = $secret_password . $identificator;
                $user = User::create([
                    'name' => $request->name,
                    'lastname' => $request->lastname,
                    'document' => $request->dni,
                    'area_admin' => $request->selectArea,
                    'hotel_admin' => $request->selectHotel,
                    'email' => $request->email,
                    'identificator' => strtolower($identificator),
                    'password' => bcrypt(strtolower($secret_password)),
                    'aux_password' => $secret_password,
                    're_entry' => $request->reEntry,
                ]);
                $user->assignRole('worker');
                $user->suitcase()->create(['name' => 'mySuitCase']);
                DB::commit();
                $final_password = $secret_password;
            }
            
            //pdf
            $details = [
                'name' => $request->name,
                'lastname' => $request->lastname,
                'password' => $final_password,
                'url' => 'https://checkincolaboradores.aplicacionescasaandina.com',
                'qr' => 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZoAAAGaAQMAAAAMyBcgAAAABlBMVEX///8AAABVwtN+AAAACXBIWXMAAA7EAAAOxAGVKw4bAAACXUlEQVR4nO2b27HCUAhFmUkBKem0bkkpwBmuwuYRo7cAsvk4xsDKF8NjR0VoNBrtX9sV9vDvm9qheuDqFfKUCHoSmg/hUx8vXJbh73vLobcjjgomNBuy9AD0CgWkdiB5zOsOQreBsoi8rzx53A4hdFfIrrymWBq5l9DdILMYPiwAD7p4Cd0BQvXw0J8HjNB8qFkWlqwpljzrSyihwRDW02o1sZHEgpLJY02H0HRoj47iI4fd9kFDFUrGh5fQbMjTAxuJFRE0GNk0lIzN1Q3pRmgsdL7tSkb4TNOwcrJs9jgIzYdc3uyDhrkjoRQ96GNlJTQXkqwpR22rZjGTqvaD0HSo0igbDOoM0kjSvhUWQiOhPCoK5aSusqYQmg+hasSbsaozGEc9vpOEJkO1jEQoHhQ1xXuQoBERGg+pVunw276WaCRPSV19+CA0FsoWssdGYr6cTrG0XLUwQoOhiMd0ekjfW73ERByh6VANFS2+vm7Zb+KK0HxIsqNAzyxIc1+pAkRoNhTDR/URbRJWri82n3QRg9BUSELXxt4qPm10b4Y8zq2G0EzoOmisIvdQN7QqDqHhEIQr/1WupohRG6xZkzgIzYbabhIryKou4w/qjYjQdMhJBJih3xzoMn0CucykhIZCQUqrM5oLSiy0hG4BeY6874iE0Bk/t6p+47Un51RCkyF8Ike81agi6jipXJVxhEZDCpkiv+aL83VkammWGEJ3gfYuYkDq0i50Ni2M0C0gbQKXyBlyI3QPqBIFASVYZINZ+T8QQtMhZAjG0dhRgYejvzcjNBui0Wi0n/YHOm3uARBM04EAAAAASUVORK5CYII='
            ];

            $pdf = PDF::loadView('pdfs.new_user', compact('details'));

            return $pdf->download('new_user.pdf');
        }
        catch (Exception $th) {
            DB::rollBack();
            throw new Error($th);
        }
    }

    public function import_users(Request $request)
    {
        try {
            set_time_limit(0);
            /** IMPORT USERS */
            if(empty($request->file)) return response()->json(['status' => 'error', 'message' => 'Archivo vacio.'],500);
            Excel::import(new UserStartImport, $request->file);

            return $this->import_users_download();

        }catch( Exception $e ){
            return response()->json(['status' => 'error', 'message' => $e->getMessage()],500);
            //return response()->json(['status' => 'error', 'message' => 'Error, uno de los campos se encuentra incompleto o existe una correo ya registrado.'],500);
        }
    }

    public function import_users_download()
    {
        $users = User::select('users.id','users.name','users.lastname','users.aux_password','hotel_admin')->where('is_pdf',0)->orderBy('id')->get();

        if(count($users) < 1) return response()->json(['status' => 'error', 'message' => 'No existen pdfs por generar.'],500);

        /** NAME FOLDER BY DAY */
        //$timestamp = strtotime($users[0]->created_at);
        //$final_date = date('d', $timestamp) .'-'. date('m', $timestamp).'-'. date('y', $timestamp);
        $links = [];
        foreach ($users as $key => $value) {
            $this->create_pdf($value->id, $value->name, $value->lastname, $value->aux_password, $value->hotel_admin /* $final_date */);
            array_push($links, 'storage/pdf/'.$value->id.' - '.$value->hotel_admin.' - '.$value->name.' '.$value->lastname.'.pdf');
        }

        /* $pdfMerger = PDFMerger::init();
        foreach ($users as $key => $value) {
            $pdfMerger->addPDF(base_path('storage/app/public/pdf/'.$final_date.'/user-'.$value->id.'.pdf'), 'all');
        }
        $pdfMerger->merge(); */

        //$code_file = Str::uuid();
        //$code_file = date('d_m_Y___H_i_s');

        //$path = 'pdfs/'.$final_date.'/';
        //$path = 'pdfs/';
        //if(!File::exists($path)) mkdir($path,777,true);
        
        //$pdfMerger->save(public_path($path.$code_file.'.pdf'), "file");

        /** mark as generated pdf */
        User::where('is_pdf', 0)->update(['is_pdf' => 1]);

        //return response()->json(['status' => 'success', 'file' => $path.$code_file.'.pdf']);
        return response()->json(['status' => 'success', 'files' => $links]);
    }

    public function create_pdf($id, $name, $lastname, $aux_password, $hotel_admin /* $final_ date */)
    {
        $details = [
            'name' => $name,
            'lastname' => $lastname,
            'password' => $aux_password,
            'url' => 'https://checkincolaboradores.aplicacionescasaandina.com',
            'qr' => 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZoAAAGaAQMAAAAMyBcgAAAABlBMVEX///8AAABVwtN+AAAACXBIWXMAAA7EAAAOxAGVKw4bAAACXUlEQVR4nO2b27HCUAhFmUkBKem0bkkpwBmuwuYRo7cAsvk4xsDKF8NjR0VoNBrtX9sV9vDvm9qheuDqFfKUCHoSmg/hUx8vXJbh73vLobcjjgomNBuy9AD0CgWkdiB5zOsOQreBsoi8rzx53A4hdFfIrrymWBq5l9DdILMYPiwAD7p4Cd0BQvXw0J8HjNB8qFkWlqwpljzrSyihwRDW02o1sZHEgpLJY02H0HRoj47iI4fd9kFDFUrGh5fQbMjTAxuJFRE0GNk0lIzN1Q3pRmgsdL7tSkb4TNOwcrJs9jgIzYdc3uyDhrkjoRQ96GNlJTQXkqwpR22rZjGTqvaD0HSo0igbDOoM0kjSvhUWQiOhPCoK5aSusqYQmg+hasSbsaozGEc9vpOEJkO1jEQoHhQ1xXuQoBERGg+pVunw276WaCRPSV19+CA0FsoWssdGYr6cTrG0XLUwQoOhiMd0ekjfW73ERByh6VANFS2+vm7Zb+KK0HxIsqNAzyxIc1+pAkRoNhTDR/URbRJWri82n3QRg9BUSELXxt4qPm10b4Y8zq2G0EzoOmisIvdQN7QqDqHhEIQr/1WupohRG6xZkzgIzYbabhIryKou4w/qjYjQdMhJBJih3xzoMn0CucykhIZCQUqrM5oLSiy0hG4BeY6874iE0Bk/t6p+47Un51RCkyF8Ike81agi6jipXJVxhEZDCpkiv+aL83VkammWGEJ3gfYuYkDq0i50Ni2M0C0gbQKXyBlyI3QPqBIFASVYZINZ+T8QQtMhZAjG0dhRgYejvzcjNBui0Wi0n/YHOm3uARBM04EAAAAASUVORK5CYII='
        ];
        $pdf = PDF::loadView('pdfs.new_user', compact('details'));
        //Storage::put('public/pdf/'.$final_date.'/user-'.$id.'.pdf', $pdf->output());
        Storage::put('public/pdf/'.$id.' - '.$hotel_admin.' - '.$name.' '.$lastname.'.pdf', $pdf->output());
    }

    public function send_notification(Request $request) 
    {
        $request->validate([ 'ids' => 'required|array' ]);
        foreach ($request->ids as $key => $value) {
            $final_aux_password = null;
            $user = User::where('id',$value)->first();
            if($user->email) {
                if(!$user->aux_password) {
                    [$identificator, $secret_password] = $this->generate_secret_password();
                    $secret_password = $secret_password . $user->identificator;
                    $final_aux_password = $secret_password;
                } else {
                    $final_aux_password = $user->aux_password;
                }
    
                $details = [ 'aux_password' => $final_aux_password, 'full_name' => $user->name ." ".$user->lastname];
                Mail::to($user->email)->send(new SendNotificationMail($details));
                $user->password_2 = bcrypt(strtolower($final_aux_password));
                $user->aux_password = $final_aux_password;
                $user->last_notification = now();
                $user->save();
            }
        }
        return response()->json(['status' => 'success', 'message' => 'Recordatorios enviados: '.count($request->ids)]);
    }
}
