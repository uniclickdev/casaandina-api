<?php

namespace App\Http\Controllers\API\V1\User\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Models\User;
use App\Exports\AverageReportExport;
use App\Exports\ReportsExport;
use Maatwebsite\Excel\Facades\Excel;

use App\Http\Controllers\API\V1\User\UserController;

class ReportController extends Controller
{
    //Según el hotel que coloca el usuario
    private function average_compliance_by_region ($start_date, $end_date)
    {
        //Position=1: hoteles, 2: oficinas corp, 3:share service
        //Hotel 30: Oficinas Corporativas - General, 31: Share Service Center - General
        User::where('position_id', 2)->update(['hotel_id' => 30]);
        User::where('position_id', 3)->update(['hotel_id' => 31]);

        $average_norte = 0;
        $average_centro_oriente = 0;
        $average_sur = 0;
        $average_sur_este = 0;
        $average_oficinas_corporativas = 0;
        $average_share_service_center = 0;

        $average = DB::table('users')
            ->select(
                'users.id',
                DB::raw('COUNT(hotel_user.user_id) as hotels'),
                'hotels.group_tag',
                'users.compromise'
            )
            ->leftJoin('hotel_user','users.id','hotel_user.user_id')
            ->leftJoin('hotels','users.hotel_id','hotels.id')
            ->where('group_tag','<>',null)
            ->when((!empty($start_date) && !empty($end_date)), function ($query) use ($start_date, $end_date) {
                $query->whereDate('users.created_at', '>=', $start_date)
                    ->whereDate('users.created_at', '<=', $end_date);
            })
            ->groupBy(
                'users.id',
                'users.hotel_id',
                'hotels.group_tag',
                'users.compromise'
            )
            ->get();

        foreach ($average as $key => $value) {
            if($value->compromise != null) {
                $value->hotels = 10;
            }
            if($value->group_tag == 'Norte') {
                if($average_norte == 0) {
                    $average_norte = round($value->hotels / 10 * 100);
                } else {
                    $average_norte = ( $average_norte + round($value->hotels / 10 * 100) ) / 2;
                }
            }
            if($value->group_tag == 'Centro Oriente') {
                if($average_centro_oriente == 0) {
                    $average_centro_oriente = round($value->hotels / 10 * 100);
                } else {
                    $average_centro_oriente = ( $average_centro_oriente + round($value->hotels / 10 * 100) ) / 2;
                }
            }
            if($value->group_tag == 'Sur') {
                if($average_sur == 0) {
                    $average_sur = round($value->hotels / 10 * 100);
                } else {
                    $average_sur = ( $average_sur + round($value->hotels / 10) ) / 2;
                }
            }
            if($value->group_tag == 'SurEste') {
                if($average_sur_este == 0) {
                    $average_sur_este = round($value->hotels / 10 * 100);
                } else {
                    $average_sur_este = ( $average_sur_este + round($value->hotels / 10) ) / 2;
                }
            }
            if($value->group_tag == 'Oficinas Corporativas') {
                if($average_oficinas_corporativas == 0) {
                    $average_oficinas_corporativas = round($value->hotels / 10 * 100);
                } else {
                    $average_oficinas_corporativas = ( $average_oficinas_corporativas + round($value->hotels / 10) ) / 2;
                }
            }
            if($value->group_tag == 'Share Service Center') {
                if($average_share_service_center == 0) {
                    $average_share_service_center = round($value->hotels / 10 * 100);
                } else {
                    $average_share_service_center = ( $average_share_service_center + round($value->hotels / 10) ) / 2;
                }
            }
        }

        $result = [
            ['Region', 'Promedio'],
            ['Norte', round($average_norte,2).' %'],
            ['Centro Oriente', round($average_centro_oriente,2).' %'],
            ['Sur', round($average_sur,2).' %'],
            ['SurEste', round($average_sur_este,2).' %'],
            ['Oficinas Corporativas', round($average_oficinas_corporativas,2).' %'],
            ['Share Service Center', round($average_share_service_center,2).' %'],
        ];

        return $result;
    }

    private function users_incomplete ($start_date, $end_date)
    {
        $users = User::select(
                DB::raw('DATE_FORMAT(users.created_at, "%Y-%m-%d %H:%i:%s") as created_date'),
                'date_start_game',
                'date_end_game',
                'users.name',
                'lastname',
                'document',
                'email',
                DB::raw('UPPER(TRIM(LEADING " " FROM users.area_admin)) as area_admin_trimmed'),
                DB::raw('UPPER(TRIM(LEADING " " FROM users.hotel_admin)) as hotel_admin_trimmed'),
                DB::raw('IF(COUNT(hotel_user.hotel_id) = 0, "0", COUNT(hotel_user.hotel_id)) as hotels_count'),
                DB::raw('CASE 
                    WHEN COUNT(hotel_user.hotel_id) = 0 THEN "0"
                    ELSE ROUND((COUNT(hotel_user.hotel_id) / 10) * 100)
                END as progress') //Ojo, en base a 10
            )
            ->leftJoin('hotel_user','users.id','hotel_user.user_id')
            ->role('worker')
            ->whereNull('users.compromise') //No terminaron el juego
            ->when((!empty($start_date) && !empty($end_date)), function ($query) use ($start_date, $end_date) {
                $query->whereDate('users.created_at', '>=', $start_date)
                    ->whereDate('users.created_at', '<=', $end_date);
            })
            ->groupBy(
                'users.created_at',
                'users.id',
                'date_start_game',
                'date_end_game',
                'users.name',
                'lastname',
                'document',
                'email',
                'area_admin',
                'hotel_admin',
            )
            ->orderBy('hotel_admin_trimmed', 'ASC')
            ->orderBy('area_admin_trimmed', 'ASC')
            ->get();
        
        $final = [[
            'Fecha registro',
            'Fecha inicio',
            'Fecha fin',
            'Nombre',
            'Apellido',
            'DNI',
            'Correo electrónico',
            'Área admin',
            'Hotel admin',
            'Hoteles contestados',
            'Progreso (%)',
        ]];

        
        $workers_arr = $users->toArray();

        $result = array_merge($final, $workers_arr);

        return $result;
    }

    private function users_complete ($start_date, $end_date)
    {
        $users = User::select(
                DB::raw('DATE_FORMAT(users.created_at, "%Y-%m-%d %H:%i:%s") as created_date'),
                'date_start_game',
                'date_end_game',
                'users.name',
                'lastname',
                'document',
                'email',
                DB::raw('UPPER(TRIM(LEADING " " FROM users.area_admin)) as area_admin_trimmed'),
                DB::raw('UPPER(TRIM(LEADING " " FROM users.hotel_admin)) as hotel_admin_trimmed'),
                DB::raw('"100 %" as percentage'),
                // DB::raw('IF(COUNT(hotel_user.hotel_id) = 0, "0", COUNT(hotel_user.hotel_id)) as hotels_count'),
            )
            // ->leftJoin('hotel_user','users.id','hotel_user.user_id')
            ->role('worker')
            ->whereNotNull('users.compromise') //Terminaron el juego
            ->when((!empty($start_date) && !empty($end_date)), function ($query) use ($start_date, $end_date) {
                $query->whereDate('users.created_at', '>=', $start_date)
                    ->whereDate('users.created_at', '<=', $end_date);
            })
            // ->groupBy(
            //     'users.created_at',
            //     'users.id',
            //     'date_start_game',
            //     'date_end_game',
            //     'users.name',
            //     'lastname',
            //     'document',
            //     'email',
            //     'area_admin',
            //     'hotel_admin',
            // )
            ->orderBy('hotel_admin_trimmed', 'ASC')
            ->orderBy('area_admin_trimmed', 'ASC')
            ->get();
        
        $final = [[
            'Fecha registro',
            'Fecha inicio',
            'Fecha fin',
            'Nombre',
            'Apellido',
            'DNI',
            'Correo electrónico',
            'Área admin',
            'Hotel admin',
            'Progreso'
            // 'Hoteles contestados',
        ]];

        $workers_arr = $users->toArray();

        $result = array_merge($final, $workers_arr);

        return $result;
    }

    //Como index, tabla admin
    private function respuestas($start_date, $end_date)
    {
        $workers = User::select(
                DB::raw('DATE_FORMAT(users.created_at, "%Y-%m-%d %H:%i:%s") as created_date'),
                'users.date_start_game',
                'users.date_end_game',
                'users.name',
                'users.lastname',
                'users.email',
                'users.document',
                DB::raw('UPPER(TRIM(LEADING " " FROM users.area_admin)) as area_admin_trimmed'),
                DB::raw('UPPER(TRIM(LEADING " " FROM users.hotel_admin)) as hotel_admin_trimmed'),
                'positions.name as area_trabajo', //area_trabajo
                DB::raw("CASE WHEN users.position_id IN (2, 3)
                    THEN '-'
                    ELSE hotels.name END as hotel_trabajo"),
                'strengths.name as fortaleza',
                'users.experience', //puntaje
                'users.compromise',
                DB::raw('
                    (SELECT COUNT(*)
                        FROM hotel_user 
                        WHERE user_id = users.id) 
                    as num_hoteles'),
                'users.last_notification',
                'users.re_entry'
            )
            ->role('worker')
            ->leftJoin('positions', 'users.position_id', 'positions.id')
            ->leftJoin('strengths', 'users.strength_id', 'strengths.id')
            ->leftJoin('hotels', 'users.hotel_id', 'hotels.id')
            ->when((!empty($start_date) && !empty($end_date)), function ($query) use ($start_date, $end_date) {
                $query->whereDate('users.created_at', '>=', $start_date)
                    ->whereDate('users.created_at', '<=', $end_date);
            })
            // ->orderBy('users.id', 'desc')
            ->orderBy('hotel_admin_trimmed', 'ASC')
            ->orderBy('area_admin_trimmed', 'ASC')
            ->orderBy('lastname', 'ASC')
            ->get();

        $headings = [[
            'Fecha registro',
            'Fecha inicio',
            'Fecha fin',
            'Nombre',
            'Apellido',
            'Correo electrónico',
            'DNI',
            'Área admin',
            'Hotel admin',
            'Área de trabajo',
            'Hotel de trabajo',
            'Fortaleza',
            'Puntaje',
            'Compromiso',
            'Hoteles contestados', //nro de hotels by worker
            'Última notificación',
            'Reingreso'
        ]];

        $workers_arr = $workers->toArray();

        $result = array_merge($headings, $workers_arr);

        return $result;
    }

    public function download_reports(Request $request)
    {
        $request->validate([
            'dateStart' => 'nullable|date',
            'dateEnd' => 'nullable|date'
        ]);

        $start_date = $request->has('dateStart') ? $request->dateStart : null;
        $end_date = $request->has('dateEnd') ? $request->dateEnd : null;

        $average_compliance_by_region = $this->average_compliance_by_region($start_date, $end_date);
        $users_incomplete = $this->users_incomplete($start_date, $end_date);
        $users_complete = $this->users_complete($start_date, $end_date);
        $respuestas = $this->respuestas($start_date, $end_date);

        return (new ReportsExport(
                $start_date,
                $end_date,
                $average_compliance_by_region,
                $users_complete,
                $users_incomplete,
                $respuestas
            ))->download('Reports.xlsx'); 
    }
}
