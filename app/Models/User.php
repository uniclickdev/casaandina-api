<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;


class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'document',
        'email',
        'password',
        'lastname',
        'gender',
        'hotel_id',
        'position',
        'position_id',
        'strength',
        'compromise',
        'score_total',
        'identificator',
        'experience',
        'date_start_game',
        'date_end_game',
        'aux_password',
        'is_pdf',
        'area_admin',
        'hotel_admin',
        're_entry',
        'created_at'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function hotels()
    {
        return $this->belongsToMany(Hotel::class)->withPivot('score', 'coins');
    }
    public function worker_hotel()
    {
        return $this->belongsTo(Hotel::class, 'hotel_id');
    }
    public function position()
    {
        return $this->belongsTo(Position::class);
    }
    public function strength()
    {
        return $this->belongsTo(Strength::class);
    }
    public function suitcase()
    {
        return $this->hasOne(Suitcase::class);
    }
}
