<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        'region_id',
        'name',
        'departament'
    ];
    public function region()
    {
        return $this->belongsTo(Region::class);
    }
    public function hotels()
    {
        return $this->hasMany(Hotel::class);
    }
}
