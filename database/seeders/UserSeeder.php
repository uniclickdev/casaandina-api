<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Eliminar los usuarios solicitados
        $emailsToDelete = [
            'cpacheco@casa-andina.com',
            'vavalos@casa-andina.com',
            'asanchezc@casa-andina.com'
        ];

        foreach ($emailsToDelete as $email) {
            $user = User::where('email', $email)->first();
            if ($user) {
                $user->delete();
                echo "Usuario con email {$email} eliminado.\n";
            } else {
                echo "Usuario con email {$email} no encontrado.\n";
            }
        }

        // Crear un nuevo usuario con una contraseña fácil de recordar
        $newUser = User::create([
            'name' => 'Nicolas Puma Chavez',
            'email' => 'nponce@casa-andina.com',
            'password' => bcrypt('nicolas123'), // Contraseña fácil de recordar
        ]);
        $newUser->assignRole('admin');

        echo "Usuario creado: Nicolas Puma Chavez (nponce@casa-andina.com).\n";
    }
}
