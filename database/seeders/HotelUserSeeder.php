<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class HotelUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::find(2);
        $user->hotels()->attach(1,[
            'score'=>100,
            'coins'=>30
        ]);
        $user->hotels()->attach(2,[
            'score'=>50,
            'coins'=>20
        ]);
        $user->hotels()->attach(3,[
            'score'=>50,
            'coins'=>20
        ]);
    }
}
