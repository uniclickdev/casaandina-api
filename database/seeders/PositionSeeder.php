<?php

namespace Database\Seeders;

use App\Models\Position;
use Illuminate\Database\Seeder;

class PositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*Position::create(['name' => 'almacenero']);
        Position::create(['name' => 'analista']);
        Position::create(['name' => 'analista junior']);
        Position::create(['name' => 'analista senior']);
        Position::create(['name' => 'anfitrion/a']);
        Position::create(['name' => 'areas publicas']);
        Position::create(['name' => 'asistente']);
        Position::create(['name' => 'asistente senior']);
        Position::create(['name' => 'auditor']);
        Position::create(['name' => 'auxiliar']);
        Position::create(['name' => 'ayudante']);
        Position::create(['name' => 'barista']);
        Position::create(['name' => 'barman']);
        Position::create(['name' => 'botones']);
        Position::create(['name' => 'cajero']);
        Position::create(['name' => 'camarero/a']);
        Position::create(['name' => 'camarero/a senior']);
        Position::create(['name' => 'capitan']);
        Position::create(['name' => 'ceo']);
        Position::create(['name' => 'chofer']);
        Position::create(['name' => 'cocinero']);
        Position::create(['name' => 'comprador']);
        Position::create(['name' => 'comprador junior']);
        Position::create(['name' => 'comprador senior']);
        Position::create(['name' => 'concierge']);
        Position::create(['name' => 'coordinador']);
        Position::create(['name' => 'desarrollador']);
        Position::create(['name' => 'director']);
        Position::create(['name' => 'diseñador']);
        Position::create(['name' => 'doorman']);
        Position::create(['name' => 'ejecutivo']);
        Position::create(['name' => 'ejecutivo junior']);
        Position::create(['name' => 'ejecutivo senior']);
        Position::create(['name' => 'especialsita']);
        Position::create(['name' => 'gerente']);
        Position::create(['name' => 'gerente de hotel']);
        Position::create(['name' => 'gerente regional']);
        Position::create(['name' => 'jardinero']);
        Position::create(['name' => 'jefe']);
        Position::create(['name' => 'lavandero/a']);
        Position::create(['name' => 'medico ocupacional']);
        Position::create(['name' => 'oficial de seguridad']);
        Position::create(['name' => 'operador de help center']);
        Position::create(['name' => 'panadero']);
        Position::create(['name' => 'pastelero']);
        Position::create(['name' => 'porcionador']);
        Position::create(['name' => 'programador']);
        Position::create(['name' => 'recepcionista']);
        Position::create(['name' => 'recepcionista senior']);
        Position::create(['name' => 'server']);
        Position::create(['name' => 'steward']);
        Position::create(['name' => 'subgerente']);
        Position::create(['name' => 'supervisor']);*/

        Position::create(['name' => 'hoteles']);
        Position::create(['name' => 'oficinas corporativas']);
        Position::create(['name' => 'share service center']);
        
    }
}
