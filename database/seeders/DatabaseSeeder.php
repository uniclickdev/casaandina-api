<?php

namespace Database\Seeders;

use App\Models\Region;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleSeeder::class);
        $this->call(PositionSeeder::class);
        $this->call(StrengthSeeder::class);
        $this->call(ThingSeeder::class);
        $this->call(HotelSeeder::class);
        $this->call(UserSeeder::class);
        // $this->call(HotelUserSeeder::class);

    }
}
