<?php

namespace Database\Seeders;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::create(['name' => 'admin']);
        $worker = Role::create(['name' => 'worker']);

        Permission::create(['name' => 'view users'])->assignRole($admin);
        Permission::create(['name' => 'view workers'])->assignRole($admin);
        Permission::create(['name' => 'view worker'])->syncRoles($admin, $worker);
        Permission::create(['name' => 'view user'])->syncRoles($admin, $worker);
        Permission::create(['name' => 'update user'])->assignRole($worker);
        Permission::create(['name' => 'update user compromise'])->assignRole($worker);

        Permission::create(['name' => 'create secret password'])->assignRole($admin);

        Permission::create(['name' => 'view hotels'])->assignRole([$admin,$worker]);
        Permission::create(['name' => 'view hotel'])->assignRole([$admin,$worker]);
        Permission::create(['name' => 'destroy hotel'])->assignRole($admin);

        Permission::create(['name' => 'view regions'])->assignRole($admin);
        Permission::create(['name' => 'view region'])->assignRole($admin);
        Permission::create(['name' => 'create region'])->assignRole($admin);
        Permission::create(['name' => 'update region'])->assignRole($admin);
        Permission::create(['name' => 'destroy region'])->assignRole($admin);
        Permission::create(['name' => 'view region hotels'])->assignRole($worker);
        Permission::create(['name' => 'view region hotel'])->assignRole($worker);

        Permission::create(['name' => 'view cities'])->assignRole($admin);
        Permission::create(['name' => 'view city'])->assignRole($admin);
        Permission::create(['name' => 'create city'])->assignRole($admin);
        Permission::create(['name' => 'update city'])->assignRole($admin);
        Permission::create(['name' => 'destroy city'])->assignRole($admin);

        Permission::create(['name' => 'view score hotels'])->assignRole($worker);
        Permission::create(['name' => 'view score hotel'])->assignRole($worker);
        Permission::create(['name' => 'update score hotel'])->assignRole($worker);

        Permission::create(['name' => 'view positions'])->assignRole($worker);
        Permission::create(['name' => 'view strengths'])->assignRole($worker);

        Permission::create(['name' => 'view things'])->assignRole($worker);
        Permission::create(['name' => 'view categories'])->assignRole($worker);
        Permission::create(['name' => 'view category things'])->assignRole($worker);
        Permission::create(['name' => 'view suitcase'])->assignRole($worker);
        Permission::create(['name' => 'update suitcase'])->assignRole($worker);
        
    }
}
