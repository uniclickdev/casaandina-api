<?php

namespace Database\Seeders;

use App\Models\Strength;
use Illuminate\Database\Seeder;

class StrengthSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Strength::create(['name' => 'integridad']);
        Strength::create(['name' => 'vocación de servicio']);
        Strength::create(['name' => 'atención al detalle']);
        Strength::create(['name' => 'trabajo en equipo']);
        Strength::create(['name' => 'espíritu de superación']);
        Strength::create(['name' => 'pasión']);
        Strength::create(['name' => 'buena onda']);
        Strength::create(['name' => 'proactividad']);
        Strength::create(['name' => 'experto del destino']);
        Strength::create(['name' => 'creatividad']);
    }
}
