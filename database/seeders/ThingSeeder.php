<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class ThingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category_one = Category::create(['name' => 'Ventanas']);
        $category_two = Category::create(['name' => 'Pergaminos']);
        $category_three = Category::create(['name' => 'Beneficios']);

        $category_one->things()->createMany(
            [
                ['name' => 'Integridad'],
                ['name' => 'Vocación de Servicio'],
                ['name' => 'Atención al Detalle'],
                ['name' => 'Trabajo en Equipo'],
                ['name' => 'Espiritu de Superación'],
                ['name' => 'Pasión'],
                ['name' => 'Buena Onda'],
                ['name' => 'Proactividad'],
                ['name' => 'Experto del destino'],
                ['name' => 'Creatividad'],
            ]
        );

        $category_two->things()->createMany(
            [
                ['name' => 'Centro Oriente'],
                ['name' => 'Norte'],
                ['name' => 'Sureste'],
                ['name' => 'Sur'],
            ]
        );

        $category_three->things()->createMany(
            [
                ['name' => 'Alimentación'],
                ['name' => 'Salud'],
                ['name' => 'Cobertura de Eps'],
                ['name' => '5 dias'],
                ['name' => 'Trámite de subsidio'],
                ['name' => 'Préstamos sin interes'],
                ['name' => 'Pago Quincena'],
                ['name' => 'Pago fin de mes'],
                ['name' => '25% descuento'],
                ['name' => 'Tarifa especial'],
                ['name' => 'Gratificación'],
                ['name' => 'Cts'],
                ['name' => 'Plan de Entrenamiento'],
                ['name' => 'Línea de carrera'],
                ['name' => 'Beneficios intercorrp'],
                ['name' => 'Seguro oncológico'],
                ['name' => 'Servicio educativos'],
                ['name' => 'Plan de vida'],
                ['name' => 'Integración'],
                ['name' => 'Voluntariado'],
            ]
        );
    }
}
