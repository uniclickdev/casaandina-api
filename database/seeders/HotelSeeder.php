<?php

namespace Database\Seeders;

use App\Models\Hotel;
use App\Models\Region;
use Illuminate\Database\Seeder;

class HotelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Regions
        $norte = Region::create(['name' => 'Norte']);
        $centro_oriente = Region::create(['name' => 'Centro Oriente']);
        $sur = Region::create(['name' => 'Sur']);
        $sur_este = Region::create(['name' => 'SurEste']);

        // Cities
        $zorritos = $norte->cities()->create(['name' => 'Zorritos Tumbes', 'departament' => 'Tumbes']);
        $talara = $norte->cities()->create(['name' => 'Talara', 'departament' => 'Piura']);
        $piura = $norte->cities()->create(['name' => 'Piura', 'departament' => 'Piura']);
        $chilcayo = $norte->cities()->create(['name' => 'Chiclayo', 'departament' => 'Lambayeque']);
        $trujillo = $norte->cities()->create(['name' => 'Trujillo Plaza', 'departament' => 'La Libertad']);

        $chincha = $centro_oriente->cities()->create(['name' => 'Chincha', 'departament' => 'Ica']);
        $san_isidro = $centro_oriente->cities()->create(['name' => 'San Isidro', 'departament' => 'Lima']);
        $miraflores = $centro_oriente->cities()->create(['name' => 'Miraflores', 'departament' => 'Lima']);
        $benavides = $centro_oriente->cities()->create(['name' => 'Benavides', 'departament' => 'Lima']);
        $miraflores_centro = $centro_oriente->cities()->create(['name' => 'Miraflores Centro', 'departament' => 'Lima']);
        $nasca = $centro_oriente->cities()->create(['name' => 'Nasca', 'departament' => 'Ica']);
        $pucallpa = $centro_oriente->cities()->create(['name' => 'Pucallpa', 'departament' => 'Ucayali']);

        $arequipa_plaza = $sur->cities()->create(['name' => 'Arequipa Plaza', 'departament' => 'Arequipa']);
        $arequipa = $sur->cities()->create(['name' => 'Arequipa', 'departament' => 'Arequipa']);
        $colca = $sur->cities()->create(['name' => 'Colca', 'departament' => 'Arequipa']);
        $tacna = $sur->cities()->create(['name' => 'Tacna', 'departament' => 'Tacna']);
        $moquegua = $sur->cities()->create(['name' => 'Moquegua', 'departament' => 'Moquegua']);

        // $arequipa_sur = $sur_este->cities()->create(['name' => 'Arequipa', 'departament' => 'Arequipa']);
        $machu_piccchu = $sur_este->cities()->create(['name' => 'Machu Picchu', 'departament' => 'Cusco']);
        $cusco_catedral = $sur_este->cities()->create(['name' => 'Cusco Catedral', 'departament' => 'Cusco']);
        $cusco_plaza = $sur_este->cities()->create(['name' => 'Cusco Plaza', 'departament' => 'Cusco']);
        $cusco_san_blas = $sur_este->cities()->create(['name' => 'Cusco San Blas', 'departament' => 'Cusco']);
        $cusco = $sur_este->cities()->create(['name' => 'Cusco', 'departament' => 'Cusco']);
        $valle_sagrado = $sur_este->cities()->create(['name' => 'Valle Sagrado Hotel & Villas', 'departament' => 'Cusco']);
        $puno = $sur_este->cities()->create(['name' => 'Puno', 'departament' => 'Puno']);

        // hotels
        $piura->hotels()->create([
            'name' => $piura->region->name . " - " . "Premium" . " - " . $piura->name,
            'service' => 'Premium',
            'description' => 'Casa Andina Premium Piura tiene una ubicación privilegiada en una zona residencial de Piura por su fácil acceso, seguridad y cercanía al centro financiero.'
        ]);
        $chilcayo->hotels()->create([
            'name' => $chilcayo->region->name . " - " . "Select" . " - " . $chilcayo->name,
            'service' => 'Select',
            'description' => 'Casa Andina Select Chiclayo ofrece a nuestros huéspedes una excelente ubicación, a solo minutos del centro de la ciudad y del aeropuerto.',
        ]);
        $zorritos->hotels()->create([
            'name' => $zorritos->region->name . " - " . "Select" . " - " . $zorritos->name,
            'service' => 'Select',
            'description' => 'Ubicado a tan solo 40 minutos del aeropuerto de Tumbes, es una excelente opción tanto para quienes viajan por negocios como para quienes llegan en búsqueda del sol y el descanso.',
        ]);
        $trujillo->hotels()->create([
            'name' => $trujillo->region->name . " - " . "Standard" . " - " . $trujillo->name,
            'service' => 'Standard',
            'description' => 'Cuenta con una ubicación estratégica a tan solo 1 cuadra de la Plaza de Armas de Trujillo y a 25 minutos del aeropuerto. Con 46 cómodas y funcionales habitaciones.',
        ]);
        $piura->hotels()->create([
            'name' => $piura->region->name . " - " . "Standard" . " - " . $piura->name,
            'service' => 'Standard',
            'description' => 'Cuenta con una ubicación estratégica por ser parte del nuevo centro financiero de Piura. Colinda con nuestro Casa Andina Premium Piura, motivo por el cual podrán hacer uso de sus modernas instalaciones.',
        ]);
        $talara->hotels()->create([
            'name' => $talara->region->name . " - " . "Standard" . " - " . $talara->name,
            'service' => 'Standard',
            'description' => 'Casa Andina Standard Talara es un hotel recientemente remodelado de cuatro pisos que cuenta con una moderna decoración y excelente iluminación y ubicación.',
        ]);
        $miraflores->hotels()->create([
            'name' => $miraflores->region->name . " - " . "Premium" . " - " . $miraflores->name,
            'service' => 'Premium',
            'description' => 'Con una ubicación estratégica y a tan solo 45 minutos del aeropuerto, nuestro Casa Andina Premium Miraflores (Lima, Perú) ofrece todos los servicios y comodidades que el huésped necesita.',
        ]);
        $san_isidro->hotels()->create([
            'name' => $san_isidro->region->name . " - " . "Premium" . " - " . $san_isidro->name,
            'service' => 'Premium',
            'description' => 'Con una ubicación estratégica en el centro empresarial de San Isidro, a 40 minutos del aeropuerto Jorge Chavez y a pocos minutos de las principales vias del centro de la ciudad.',
        ]);
        $miraflores->hotels()->create([
            'name' => $miraflores->region->name . " - " . "Select" . " - " . $miraflores->name,
            'service' => 'Select',
            'description' => 'Nuestro Casa Andina Select Miraflores (Lima, Perú) se encuentra ubicado en la avenida Schell, a 2 cuadras de la avenida Larco, una de las más importantes de Miraflores.',
        ]);
        $pucallpa->hotels()->create([
            'name' => $pucallpa->region->name . " - " . "Select" . " - " . $pucallpa->name,
            'service' => 'Select',
            'description' => 'Es un hotel moderno, funcional y se encuentra a tan solo 15 minutos del aeropuerto con infraestructura de primera y una ubicación estratégica.',
        ]);
        $benavides->hotels()->create([
            'name' => $benavides->region->name . " - " . "Standard" . " - " . $benavides->name,
            'service' => 'Standard',
            'description' => 'Avenida Alfredo Benavides 271- Miraflores',
        ]);
        $chincha->hotels()->create([
            'name' => $chincha->region->name . " - " . "Standard" . " - " . $chincha->name,
            'service' => 'Standard',
            'description' => 'Ubicado a pocos minutos del corazón de la ciudad. Un lugar ideal para viajeros que quieren descansar de la rutina y relajarse en un ambiente tranquilo.',
        ]);
        $miraflores_centro->hotels()->create([
            'name' => $miraflores_centro->region->name . " - " . "Standard" . " - " . $miraflores_centro->name,
            'service' => 'Standard',
            'description' => 'Es un práctico y cómodo hotel con una ubicación privilegiada ya que se encuentra en la principal zona turística de Lima: el distrito de Miraflores.',
        ]);
        $nasca->hotels()->create([
            'name' => $nasca->region->name . " - " . "Standard" . " - " . $nasca->name,
            'service' => 'Standard',
            'description' => 'Es un hotel práctico y seguro, cuenta con 61 habitaciones 100% no fumadoras distribuidas en 3 pisos. Tiene una excelente ubicación, a tan solo unas cuadras de la Plaza de Armas de Nasca.',
        ]);
        $arequipa->hotels()->create([
            'name' => $arequipa->region->name . " - " . "Premium" . " - " . $arequipa->name,
            'service' => 'Premium',
            'description' => 'Ubicado estratégicamente a solo unas cuadras de la Plaza de Armas de Arequipa, nuestro Casa Andina Premium Arequipa es la opción ideal para viajes de negocios y turismo.',
        ]);
        $arequipa->hotels()->create([
            'name' => $arequipa->region->name . " - " . "Standard" . " - " . $arequipa->name,
            'service' => 'Standard',
            'description' => 'Tiene una ubicación estratégica 5 minutos de la Plaza de Armas en auto. Cuenta con 105 cómodas y prácticas habitaciones con acabados impecables.',
        ]);
        $colca->hotels()->create([
            'name' => $colca->region->name . " - " . "Standard" . " - " . $colca->name,
            'service' => 'Standard',
            'description' => 'Un hotel rústico, relajado e íntimo ubicado en Chivay. Con 51 habitaciones 100% no fumadoras, levantadas a manera de casitas de piedra, con pisos de madera.',
        ]);
        $arequipa_plaza->hotels()->create([
            'name' => $arequipa_plaza->region->name . " - " . "Select" . " - " . $arequipa_plaza->name,
            'service' => 'Select',
            'description' => 'Nuestro Casa Andina Select Arequipa Plaza es el tercer hotel de la cadena en la ciudad. Con una ubicación estratégica, frente a la Plaza de Armas en el centro histórico de la ciudad es el lugar ideal para obtener las mejores vistas.',
        ]);
        $moquegua->hotels()->create([
            'name' => $moquegua->region->name . " - " . "Select" . " - " . $moquegua->name,
            'service' => 'Select',
            'description' => 'Un hotel con un diseño moderno y funcional, la combinación perfecta entre comodidad y atención al detalle. Ubicado a pocos minutos del centro de la ciudad.',
        ]);
        $tacna->hotels()->create([
            'name' => $tacna->region->name . " - " . "Select" . " - " . $tacna->name,
            'service' => 'Select',
            'description' => 'Ubicado estratégicamente a 15 minutos del aeropuerto de Tacna. Cuenta con 150 habitaciones funcionales distribuidas en 9 pisos.',
        ]);
        $cusco->hotels()->create([
            'name' => $cusco->region->name . " - " . "Premium" . " - " . $cusco->name,
            'service' => 'Premium',
            'description' => 'Ubicado a 5 minutos de la Plaza de Armas de la ciudad y a pocos pasos de los atractivos turísticos de la ciudad. Una hermosa casona que te transportará en el tiempo gracias a sus patios y balcones coloniales.',
        ]);
        $puno->hotels()->create([
            'name' => $puno->region->name . " - " . "Premium" . " - " . $puno->name,
            'service' => 'Premium',
            'description' => 'A unos pasos del Lago Titicaca. Ideal si buscas una experiencia con vistas paisajísticas naturales espectaculares.',
        ]);
        $valle_sagrado->hotels()->create([
            'name' => $valle_sagrado->region->name . " - " . "Premium" . " - " . $valle_sagrado->name,
            'service' => 'Premium',
            'description' => 'Con una ubicación privilegiada, cuenta con fabulosas vistas de los Andes desde todas las habitaciones.',
        ]);
        $puno->hotels()->create([
            'name' => $puno->region->name . " - " . "Standard" . " - " . $puno->name,
            'service' => 'Standard',
            'description' => 'Nuestro Casa Andina Standard Puno es un hotel cómodo, seguro y tranquilo, con espaciosas áreas comunes, incluyendo una sala de estar con una chimenea y un patio.',
        ]);
        $cusco_catedral->hotels()->create([
            'name' => $cusco_catedral->region->name . " - " . "Standard" . " - " . $cusco_catedral->name,
            'service' => 'Standard',
            'description' => 'Es un cómodo y acogedor hotel ubicado a unos pasos de la Catedral Principal. Su ubicación estratégica, en una esquina de la Plaza de Armas, permiten a sus visitantes disfrutar de las principales vistas y alrededores.',
        ]);
        $cusco->hotels()->create([
            'name' => $cusco->region->name . " - " . "Standard" . " - " . $cusco->name,
            'service' => 'Standard',
            'description' => 'Es un hotel cómodo y acogedor de 35 habitaciones 100% no fumadoras con una ubicación estratégica a tan solo 4 cuadras de la Plaza Mayor de la ciudad y al Templo Koricancha.',
        ]);
        $cusco_plaza->hotels()->create([
            'name' => $cusco_plaza->region->name . " - " . "Standard" . " - " . $cusco_plaza->name,
            'service' => 'Standard',
            'description' => 'Es un práctico y cómodo hotel ubicado a tan solo una cuadra de la Plaza de Armas. Desde aquí podrás acceder fácilmente a los mejores lugares del destino como: la Catedral, la Plaza Regocijo y tener las mejores vistas.',
        ]);
        $cusco_san_blas->hotels()->create([
            'name' => $cusco_san_blas->region->name . " - " . "Standard" . " - " . $cusco_san_blas->name,
            'service' => 'Standard',
            'description' => 'Ubicado en el barrio de San Blas apartado de la zona agitada de Cusco. Alberga 41 cómodas y acogedoras habitaciones 100% no fumadoras levantadas en torno a un antiguo patio de piedra.',
        ]);
        $machu_piccchu->hotels()->create([
            'name' => $machu_piccchu->region->name . " - " . "Standard" . " - " . $machu_piccchu->name,
            'service' => 'Standard',
            'description' => 'El hotel cuenta con 53 nuevas y cómodas habitaciones con acabados básicos impecables que te permitirá tener la tranquilidad necesaria descansar y reponer energías.',
        ]);
    }
}
