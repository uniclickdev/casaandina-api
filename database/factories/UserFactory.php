<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'lastname' => $this->faker->name(),
            'gender' => 'Masculino',
            'hotel_id' => rand(1, 5),
            'position_id' => rand(1, 3),
            'strength_id' => rand(1, 5),
            'email' => $this->faker->unique()->safeEmail(),
            'email_verified_at' => now(),
            // 'identificator' =>  Str::random(2),
            'password' => bcrypt('password'), // password
            'remember_token' => Str::random(10),
            'date_start_game' => $this->faker->dateTimeBetween($startDate = '-2 day', $endDate = '-1 day'),
            'date_end_game' => $this->faker->dateTimeBetween($startDate = '-1 day', $endDate = 'now'),
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}
