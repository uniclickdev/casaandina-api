<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuitcaseThingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suitcase_thing', function (Blueprint $table) {
            $table->foreignId('suitcase_id')->constrained('suitcases');
            $table->foreignId('thing_id')->constrained('things');
            $table->primary(['suitcase_id','thing_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suitcase_thing');
    }
}
