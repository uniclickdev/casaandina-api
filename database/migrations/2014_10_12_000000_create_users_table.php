<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name', 150)->nullable();
            $table->string('lastname', 150)->nullable();
            $table->string('gender', 50)->nullable();
            $table->string('document')->nullable();
            $table->foreignId('hotel_id')->nullable()->constrained('hotels');
            $table->foreignId('position_id')->nullable()->constrained('positions');
            $table->foreignId('strength_id')->nullable()->constrained('strengths');
            $table->smallInteger('experience')->nullable();
            $table->text('compromise')->nullable();
            $table->timestamp('date_start_game')->nullable();
            $table->timestamp('date_end_game')->nullable();
            $table->string('email')->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('identificator', 5)->nullable()->unique();
            $table->string('password')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
