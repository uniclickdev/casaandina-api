<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHotelUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel_user', function (Blueprint $table) {
            // $table->id();
            $table->foreignId('hotel_id')->constrained('hotels');
            $table->foreignId('user_id')->constrained('users');
            $table->smallInteger('score')->nullable();
            $table->smallInteger('coins')->nullable();

            $table->primary(['hotel_id','user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotel_user');
    }
}
