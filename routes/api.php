<?php

use App\Http\Controllers\API\V1\AuthController;
use App\Http\Controllers\API\V1\Hotel\CityController;
use App\Http\Controllers\API\V1\Hotel\HotelController;
use App\Http\Controllers\API\V1\Hotel\RegionController;
use App\Http\Controllers\API\V1\HotelUser\HotelUserController;
use App\Http\Controllers\API\V1\User\PositionController;
use App\Http\Controllers\API\V1\User\StrengthController;
use App\Http\Controllers\API\V1\User\Suitcase\CategoryController;
use App\Http\Controllers\API\V1\User\Suitcase\ThingController;
use App\Http\Controllers\API\V1\User\UserController;
use App\Http\Controllers\API\V1\User\Report\ReportController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::post('v1/login', [AuthController::class, 'login']);
Route::post('v1/login/secret-password', [AuthController::class, 'login_secret_password']);
Route::post('v1/register', [AuthController::class, 'register']);

Route::get('v1/users/secret-password-pdf/import-users-download', [UserController::class, 'import_users_download']);


Route::middleware(['auth:sanctum'])->group(function () {
    Route::post('v1/logout', [AuthController::class, 'logout']);

    Route::get('v1/users', [UserController::class, 'index'])->middleware('permission:view users');
    
    //tabla admin
    Route::get('v1/workers', [UserController::class, 'index_workers'])->middleware('permission:view workers');
    
    Route::get('v1/workers/{user}', [UserController::class, 'show_worker'])->middleware('permission:view worker');
    
    Route::post('v1/users/secret-password-pdf/import-users', [UserController::class, 'import_users'])->middleware('permission:create secret password');
    Route::post('v1/users/secret-password-pdf', [UserController::class, 'store_secret_password_pdf'])->middleware('permission:create secret password');
    Route::post('v1/users/send-notification', [UserController::class, 'send_notification'])->middleware('permission:create secret password');
    Route::post('v1/users/secret-password', [UserController::class, 'store_secret_password'])->middleware('permission:create secret password');
    Route::post('v1/users/excel', [ReportController::class, 'download_reports'])->middleware('permission:create secret password');

    Route::patch('v1/users/{user}/compromise', [UserController::class, 'update_compromise'])->middleware('permission:update user compromise');
    Route::get('v1/users/{user}', [UserController::class, 'show'])->middleware('permission:view user');
    Route::put('v1/users/{user}', [UserController::class, 'update'])->middleware('permission:update user');

    // Route::post('v1/users/average-compliance', [ReportController::class, 'average_compliance_by_region'])->middleware('permission:create secret password');
    // Route::post('v1/users/users-incomplete', [ReportController::class, 'users_incomplete'])->middleware('permission:create secret password');
    // Route::post('v1/users/users-complete', [ReportController::class, 'users_complete'])->middleware('permission:create secret password');

    Route::get('v1/users/{user}/hotels', [HotelUserController::class, 'index'])->middleware('permission:view score hotels');
    Route::get('v1/users/{user}/hotels/{hotel}', [HotelUserController::class, 'show'])->middleware('permission:view score hotel');
    Route::put('v1/users/{user}/hotels/{hotel}', [HotelUserController::class, 'update'])->middleware('permission:update score hotel');
    Route::get('v1/users/{user}/suitcase', [UserController::class, 'show_suitcase'])->middleware('permission:view suitcase');
    Route::put('v1/users/{user}/suitcase', [UserController::class, 'update_suitcase'])->middleware('permission:update suitcase');

    Route::get('v1/hotels', [HotelController::class, 'index'])->middleware('permission:view hotels');
    Route::get('v1/positions_admin', [PositionController::class, 'index'])->middleware('permission:view hotels');

    Route::get('v1/hotels/{hotel}', [HotelController::class, 'show'])->middleware('permission:view hotel');
    Route::delete('v1/hotels/{hotel}', [HotelController::class, 'destroy'])->middleware('permission:destroy hotel');

    Route::get('v1/regions', [RegionController::class, 'index'])->middleware('permission:view regions');
    Route::get('v1/regions/{region}', [RegionController::class, 'show'])->middleware('permission:view region');
    Route::post('v1/regions', [RegionController::class, 'store'])->middleware('permission:create region');
    Route::put('v1/regions/{region}', [RegionController::class, 'update'])->middleware('permission:update region');
    Route::delete('v1/regions/{region}', [RegionController::class, 'destroy'])->middleware('permission:destroy region');
    Route::get('v1/regions/{region}/hotels', [RegionController::class, 'index_region_hotels'])->middleware('permission:view region hotels');
    Route::get('v1/regions/{region}/hotels/{hotel}', [RegionController::class, 'show_region_hotels'])->middleware('permission:view region hotel');

    Route::get('v1/cities', [CityController::class, 'index'])->middleware('permission:view cities');
    Route::get('v1/cities/{city}', [CityController::class, 'show'])->middleware('permission:view city');
    Route::post('v1/cities', [CityController::class, 'store'])->middleware('permission:create city');
    Route::put('v1/cities/{city}', [CityController::class, 'update'])->middleware('permission:update city');
    Route::delete('v1/cities/{city}', [CityController::class, 'destroy'])->middleware('permission:destroy city');

    Route::get('v1/positions', [PositionController::class, 'index'])->middleware('permission:view positions');
    Route::get('v1/strengths', [StrengthController::class, 'index'])->middleware('permission:view strengths');

    Route::get('v1/things', [ThingController::class, 'index'])->middleware('permission:view things');
    Route::get('v1/categories', [CategoryController::class, 'index'])->middleware('permission:view categories');
    Route::get('v1/categories/{category}/things', [CategoryController::class, 'show_things'])->middleware('permission:view category things');
});
