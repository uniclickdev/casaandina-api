<?php

namespace Tests\Unit\API\V1\HotelUser;

use App\Models\City;
use App\Models\Hotel;
use App\Models\Region;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class HotelUserControllerTest extends TestCase
{
    use RefreshDatabase;
    public function test_index()
    {
        $this->withExceptionHandling();
        $worker = Role::create(['name' => 'worker']);
        Permission::create(['name' => 'view score hotels'])->assignRole($worker);
        $user = User::factory()->create(['name' => 'Luis', 'email' => 'Luis@gmail.com', 'password' => bcrypt('123456'), 'hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);
        $user->assignRole('worker');
        $this->actingAs($user)->withSession(['banned' => false])->getJson("api/v1/users/$user->id/hotels")
            ->assertStatus(200)
            ->assertJson(['data' => []]);
    }
    public function test_show()
    {
        $this->withExceptionHandling();
        $worker = Role::create(['name' => 'worker']);
        Permission::create(['name' => 'view score hotel'])->assignRole($worker);
        $user = User::factory()->create(['name' => 'Luis', 'email' => 'Luis@gmail.com', 'password' => bcrypt('123456'), 'hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);
        $user->assignRole('worker');
        
        Region::factory()
        ->has(City::factory()->count(1)->has(Hotel::factory()->count(1)))
        ->create();
        
        $user->hotels()->attach(1);
        $this->actingAs($user)->withSession(['banned' => false])->getJson("api/v1/users/$user->id/hotels/1")
            ->assertStatus(200)
            ->assertJson(['data' => []]);
    }
    public function test_update()
    {
        $this->withExceptionHandling();
        $worker = Role::create(['name' => 'worker']);
        Permission::create(['name' => 'update score hotel'])->assignRole($worker);
        $user = User::factory()->create(['name' => 'Luis', 'email' => 'Luis@gmail.com', 'password' => bcrypt('123456'), 'hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);
        $user->assignRole('worker');
        
        Region::factory()
        ->has(City::factory()->count(1)->has(Hotel::factory()->count(1)))
        ->create();

        $user->hotels()->attach(1, [
            'score' => 5,
            'coins' => 5
        ]);
        $this->actingAs($user)->withSession(['banned' => false])->putJson("api/v1/users/$user->id/hotels/1", [
            'score' => 100,
            'coins' => 500
        ])
            ->assertStatus(200)
            ->assertJson(['data' => []]);
    }
}
