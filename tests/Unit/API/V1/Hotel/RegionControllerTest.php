<?php

namespace Tests\Unit\API\V1\Hotel;

use App\Models\City;
use App\Models\Hotel;
use App\Models\Region;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RegionControllerTest extends TestCase
{
    use RefreshDatabase;
    public function test_index()
    {
        $this->withExceptionHandling();
        $admin = Role::create(['name' => 'admin']);
        Permission::create(['name' => 'view regions'])->assignRole($admin);
        $user = User::factory()->create(['name' => 'Luis', 'email' => 'Luis@gmail.com', 'password' => bcrypt('123456'), 'hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);
        $user->assignRole('admin');
        $this->actingAs($user)->withSession(['banned' => false])->getJson("api/v1/regions")
            ->assertStatus(200)
            ->assertJson(['data' => []]);
    }
    public function test_show()
    {
        $this->withExceptionHandling();
        $admin = Role::create(['name' => 'admin']);
        Permission::create(['name' => 'view region'])->assignRole($admin);
        $user = User::factory()->create(['name' => 'Luis', 'email' => 'Luis@gmail.com', 'password' => bcrypt('123456'), 'hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);
        $user->assignRole('admin');
        $region = Region::factory()->create();
        $this->actingAs($user)->withSession(['banned' => false])->getJson("api/v1/regions/$region->id")
            ->assertStatus(200)
            ->assertJson(['data' => []]);
    }
    public function test_show_not_found()
    {
        $this->withExceptionHandling();
        $user = User::factory()->create(['name' => 'Luis', 'email' => 'Luis@gmail.com', 'password' => bcrypt('123456'), 'hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);
        $this->actingAs($user)->withSession(['banned' => false])->getJson("api/v1/regions/100")
            ->assertStatus(404)
            ->assertJson(['message' => "Resource not found"]);
    }
    public function test_store()
    {
        $this->withExceptionHandling();
        $admin = Role::create(['name' => 'admin']);
        Permission::create(['name' => 'create region'])->assignRole($admin);
        $user = User::factory()->create(['name' => 'Luis', 'email' => 'Luis@gmail.com', 'password' => bcrypt('123456'), 'hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);
        $user->assignRole('admin');
        $payload = [
            'name' => 'Region Sur'
        ];
        $this->actingAs($user)->withSession(['banned' => false])->postJson("api/v1/regions", $payload)
            ->assertStatus(201)
            ->assertJson(['data' => [], 'message' => 'Region Created']);
    }
    public function test_update()
    {
        $this->withExceptionHandling();
        $admin = Role::create(['name' => 'admin']);
        Permission::create(['name' => 'update region'])->assignRole($admin);
        $user = User::factory()->create(['name' => 'Luis', 'email' => 'Luis@gmail.com', 'password' => bcrypt('123456'), 'hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);
        $user->assignRole('admin');
        $region = Region::factory()->create();
        $payload = [
            'name' => 'Region Sur'
        ];
        $this->actingAs($user)->withSession(['banned' => false])->putJson("api/v1/regions/$region->id", $payload)
            ->assertStatus(200)
            ->assertJson(['data' => [], 'message' => 'Region Updated']);
    }
    public function test_destroy()
    {
        $this->withExceptionHandling();
        $admin = Role::create(['name' => 'admin']);
        Permission::create(['name' => 'destroy region'])->assignRole($admin);
        $user = User::factory()->create(['name' => 'Luis', 'email' => 'Luis@gmail.com', 'password' => bcrypt('123456'), 'hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);
        $user->assignRole('admin');
        $region = Region::factory()->create();
        $this->actingAs($user)->withSession(['banned' => false])->deleteJson("api/v1/regions/$region->id")
            ->assertStatus(200)
            ->assertJson(['data' => [], 'message' => 'Region Destroy']);
    }
    public function test_index_region_hotels()
    {
        $this->withExceptionHandling();
        $admin = Role::create(['name' => 'admin']);
        Permission::create(['name' => 'view region hotels'])->assignRole($admin);
        $user = User::factory()->create(['name' => 'Luis', 'email' => 'Luis@gmail.com', 'password' => bcrypt('123456'), 'hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);
        $user->assignRole('admin');
        $region = Region::factory()
            ->has(City::factory()->count(1)->has(Hotel::factory()->count(1)))
            ->create();
        $this->actingAs($user)->withSession(['banned' => false])->getJson("api/v1/regions/$region->id/hotels")
            ->assertStatus(200)
            ->assertJson(['data' => []]);
    }
    public function test_show_region_hotel()
    {
        $this->withExceptionHandling();
        $admin = Role::create(['name' => 'admin']);
        Permission::create(['name' => 'view region hotel'])->assignRole($admin);
        $user = User::factory()->create(['name' => 'Luis', 'email' => 'Luis@gmail.com', 'password' => bcrypt('123456'), 'hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);
        $user->assignRole('admin');
        $region = Region::factory()
            ->has(City::factory()->count(1)->has(Hotel::factory()->count(1)))
            ->create();
        $region = Region::factory()
            ->has(City::factory()->count(1)->has(Hotel::factory()->count(1)))
            ->create();
        $this->actingAs($user)->withSession(['banned' => false])->getJson("api/v1/regions/$region->id/hotels/2")
            ->assertStatus(200)
            ->assertJson(['data' => []]);
    }
    public function test_show_not_belong_region_hotel()
    {
        $this->withExceptionHandling();
        $admin = Role::create(['name' => 'admin']);
        Permission::create(['name' => 'view region hotel'])->assignRole($admin);
        $user = User::factory()->create(['name' => 'Luis', 'email' => 'Luis@gmail.com', 'password' => bcrypt('123456'), 'hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);
        $user->assignRole('admin');
        $region = Region::factory()
            ->has(City::factory()->count(1)->has(Hotel::factory()->count(1)))
            ->create();
        $region = Region::factory()
            ->has(City::factory()->count(1)->has(Hotel::factory()->count(1)))
            ->create();
        $this->actingAs($user)->withSession(['banned' => false])->getJson("api/v1/regions/$region->id/hotels/1")
            ->assertStatus(404);
    }
}
