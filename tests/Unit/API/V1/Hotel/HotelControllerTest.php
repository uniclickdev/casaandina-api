<?php

namespace Tests\Unit\API\V1\Hotel;

use App\Models\City;
use App\Models\Hotel;
use App\Models\Region;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class HotelControllerTest extends TestCase
{
    use RefreshDatabase;
    public function test_index()
    {
        $this->withExceptionHandling();
        $admin = Role::create(['name' => 'admin']);
        Permission::create(['name' => 'view hotels'])->assignRole($admin);
        $user = User::factory()->create(['name' => 'Luis', 'email' => 'Luis@gmail.com', 'password' => bcrypt('123456'), 'hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);
        $user->assignRole('admin');
        $this->actingAs($user)->withSession(['banned' => false])->getJson("api/v1/hotels")
            ->assertStatus(200)
            ->assertJson(['data' => []]);
    }
    public function test_show()
    {
        $this->withExceptionHandling();
        $admin = Role::create(['name' => 'admin']);
        Permission::create(['name' => 'view hotel'])->assignRole($admin);
        $user = User::factory()->create(['name' => 'Luis', 'email' => 'Luis@gmail.com', 'password' => bcrypt('123456'), 'hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);
        $user->assignRole('admin');
        
        Region::factory()
        ->has(City::factory()->count(1)->has(Hotel::factory()->count(1)))
        ->create();

        $this->actingAs($user)->withSession(['banned' => false])->getJson("api/v1/hotels/1")
            ->assertStatus(200)
            ->assertJson(['data' => []]);
    }
    public function test_show_not_found()
    {
        $this->withExceptionHandling();
        $user = User::factory()->create(['name' => 'Luis', 'email' => 'Luis@gmail.com', 'password' => bcrypt('123456'), 'hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);
        $this->actingAs($user)->withSession(['banned' => false])->getJson("api/v1/hotels/100")
            ->assertStatus(404)
            ->assertJson(['message' => "Resource not found"]);
    }
}
