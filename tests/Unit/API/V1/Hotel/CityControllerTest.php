<?php

namespace Tests\Unit\API\V1\Hotel;

use App\Models\City;
use App\Models\Hotel;
use App\Models\Region;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class CityControllerTest extends TestCase
{
    use RefreshDatabase;
    public function test_index()
    {
        $this->withExceptionHandling();
        $admin = Role::create(['name' => 'admin']);
        Permission::create(['name' => 'view cities'])->assignRole($admin);
        $user = User::factory()->create(['name' => 'Luis', 'email' => 'Luis@gmail.com', 'password' => bcrypt('123456'), 'hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);
        $user->assignRole('admin');
        $this->actingAs($user)->withSession(['banned' => false])->getJson("api/v1/cities")
            ->assertStatus(200)
            ->assertJson(['data' => []]);
    }
    public function test_show()
    {
        $this->withExceptionHandling();
        $admin = Role::create(['name' => 'admin']);
        Permission::create(['name' => 'view city'])->assignRole($admin);
        $user = User::factory()->create(['name' => 'Luis', 'email' => 'Luis@gmail.com', 'password' => bcrypt('123456'), 'hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);
        $user->assignRole('admin');

        Region::factory()
            ->has(City::factory()->count(1)->has(Hotel::factory()->count(1)))
            ->create();

        $this->actingAs($user)->withSession(['banned' => false])->getJson("api/v1/cities/1")
            ->assertStatus(200)
            ->assertJson(['data' => []]);
    }
    public function test_show_not_found()
    {
        $this->withExceptionHandling();
        $user = User::factory()->create(['name' => 'Luis', 'email' => 'Luis@gmail.com', 'password' => bcrypt('123456'), 'hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);
        $this->actingAs($user)->withSession(['banned' => false])->getJson("api/v1/cities/100")
            ->assertStatus(404)
            ->assertJson(['message' => "Resource not found"]);
    }
    public function test_store()
    {
        $this->withExceptionHandling();
        $admin = Role::create(['name' => 'admin']);
        Permission::create(['name' => 'create city'])->assignRole($admin);
        $user = User::factory()->create(['name' => 'Luis', 'email' => 'Luis@gmail.com', 'password' => bcrypt('123456'), 'hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);
        $user->assignRole('admin');
        $region = Region::factory()->create();
        $payload = [
            'region_id' => 1,
            'name' => 'Region Sur',
            'departament' => 'La Libertad'
        ];
        $this->actingAs($user)->withSession(['banned' => false])->postJson("api/v1/cities", $payload)
            ->assertStatus(201)
            ->assertJson(['data' => [], 'message' => 'City Created']);
    }
    public function test_update()
    {
        $this->withExceptionHandling();
        $admin = Role::create(['name' => 'admin']);
        Permission::create(['name' => 'update city'])->assignRole($admin);
        $user = User::factory()->create(['name' => 'Luis', 'email' => 'Luis@gmail.com', 'password' => bcrypt('123456'), 'hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);
        $user->assignRole('admin');

        Region::factory()
            ->has(City::factory()->count(1)->has(Hotel::factory()->count(1)))
            ->create();

        $payload = [
            'region_id' => 1,
            'name' => 'Region Sur',
            'departament' => 'La Libertad'
        ];
        $this->actingAs($user)->withSession(['banned' => false])->putJson("api/v1/cities/1", $payload)
            ->assertStatus(200)
            ->assertJson(['data' => [], 'message' => 'City Updated']);
    }
    public function test_destroy()
    {
        $this->withExceptionHandling();
        $admin = Role::create(['name' => 'admin']);
        Permission::create(['name' => 'destroy city'])->assignRole($admin);
        $user = User::factory()->create(['name' => 'Luis', 'email' => 'Luis@gmail.com', 'password' => bcrypt('123456'), 'hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);
        $user->assignRole('admin');

        Region::factory()
            ->has(City::factory()->count(1)->has(Hotel::factory()->count(1)))
            ->create();

        $this->actingAs($user)->withSession(['banned' => false])->deleteJson("api/v1/cities/1")
            ->assertStatus(200)
            ->assertJson(['data' => [], 'message' => 'City Destroy']);
    }
}
