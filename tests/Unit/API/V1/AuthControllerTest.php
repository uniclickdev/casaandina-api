<?php

namespace Tests\Unit\API\V1;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;
use Tests\TestCase;

class AuthControllerTest extends TestCase
{
    use RefreshDatabase;
    public function test_login()
    {
        $user = User::factory()->create(['password' => bcrypt('123456'), 'hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);
        $response = $this->postJson('api/v1/login', [
            'email' => $user->email,
            'password' => '123456',
        ]);
        $response->assertOk();
    }
    public function test_login_credentials_invalid()
    {
        $user = User::factory()->create(['password' => bcrypt('123456'), 'hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);
        $response = $this->postJson('api/v1/login', [
            'email' => $user->email,
            'password' => '1234568',
        ]);
        $response->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'message' => [
                        "Credentials invalid"
                    ]
                ]
            ]);
    }
    public function test_login_not_body()
    {
        $response = $this->postJson('api/v1/login', []);
        $response->assertStatus(422)
            ->assertJson(['message' => 'The given data was invalid.']);
    }
    public function test_logout()
    {
        $user = User::factory()->create(['password' => bcrypt('123456'), 'hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);
        $response = $this->actingAs($user)->withSession(['banned' => false])->postJson('api/v1/logout');
        $response->assertOk()
            ->assertJson(['message' => 'You have successfully logged out']);
    }
    public function test_unauthenticated()
    {
        $response = $this->postJson('api/v1/logout');
        $response->assertStatus(401)
            ->assertJson(['message' => 'Unauthenticated.']);
    }
    // public function test_register()
    // {
    //     $response = $this->postJson('api/v1/register', [
    //         'name' => 'Luis',
    //         'lastname' => 'Murrugarra',
    //         'gender' => 'Masculino',
    //         'hotel' => 'Hotel Ica',
    //         'position' => 'Recepcionista',
    //         'strength' => 'Responsabilidad',
    //         'email' => 'lmurrugarra@gmail.com',
    //         'password' => '123456789',
    //     ]);
    //     $response->assertStatus(201);
    // }
    public function test_register_not_body()
    {
        $response = $this->postJson('api/v1/register', []);
        $response->assertStatus(422)
            ->assertJson(['message' => 'The given data was invalid.']);
    }
    public function test_login_with_key()
    {
        $user = User::factory()->create(['hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);

        $identificator = Str::random(2);
        $secret_password = Str::random(5 - strlen($identificator));
        $secret_password = $secret_password . $identificator;

        $user->identificator = strtolower($identificator);
        $user->password = bcrypt(strtolower($secret_password));
        $user->save();

        $response = $this->postJson('api/v1/login/secret-password', [
            'secret_password' => $secret_password,
        ]);
        $response->assertOk();
    }
}
