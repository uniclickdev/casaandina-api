<?php

namespace Tests\Unit\API\V1\User;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class StrengthControllerTest extends TestCase
{
    use RefreshDatabase;
    public function test_index()
    {
        $this->withExceptionHandling();
        $worker = Role::create(['name' => 'worker']);
        Permission::create(['name' => 'view strengths'])->assignRole($worker);
        $user = User::factory()->create(['name' => 'Luis', 'email' => 'Luis@gmail.com', 'password' => bcrypt('123456'), 'hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);
        $user->assignRole('worker');
        $this->actingAs($user)->withSession(['banned' => false])->getJson("api/v1/strengths")
            ->assertStatus(200)
            ->assertJson(['data' => []]);
    }
}
