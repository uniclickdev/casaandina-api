<?php

namespace Tests\Unit\API\V1\User\Suitcase;

use App\Models\Category;
use App\Models\Thing;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class ThingControllerTest extends TestCase
{
    use RefreshDatabase;
    public function test_index()
    {
        $this->withExceptionHandling();
        $worker = Role::create(['name' => 'worker']);
        Permission::create(['name' => 'view things'])->assignRole($worker);
        $user = User::factory()->create(['name' => 'Luis', 'email' => 'Luis@gmail.com', 'password' => bcrypt('123456'), 'hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);
        $user->assignRole('worker');

        $category = Category::factory()->create();
        Thing::factory()
            ->count(5)
            ->for($category)
            ->create();

        $this->actingAs($user)->withSession(['banned' => false])->getJson("api/v1/things")
            ->assertStatus(200)
            ->assertJson(['data' => []]);
    }
}
