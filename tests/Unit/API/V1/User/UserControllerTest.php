<?php

namespace Tests\Unit\API\V1\User;

use App\Models\Category;
use App\Models\City;
use App\Models\Hotel;
use App\Models\Position;
use App\Models\Region;
use App\Models\Strength;
use App\Models\Thing;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UserControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_index()
    {
        $this->withExceptionHandling();
        $admin = Role::create(['name' => 'admin']);
        Permission::create(['name' => 'view users'])->assignRole($admin);
        $user = User::factory()->create(['name' => 'Luis', 'email' => 'Luis@gmail.com', 'password' => bcrypt('123456'), 'hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);
        $user->assignRole('admin');
        $this->actingAs($user)->withSession(['banned' => false])->getJson("api/v1/users")
            ->assertStatus(200)
            ->assertJson(['data' => []]);
    }
    public function test_index_not_authorizate()
    {
        $this->withExceptionHandling();
        $user = User::factory()->create(['name' => 'Luis', 'email' => 'Luis@gmail.com', 'password' => bcrypt('123456'), 'hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);
        $this->actingAs($user)->withSession(['banned' => false])->getJson("api/v1/users")
            ->assertStatus(403)
            ->assertJson(['message' => 'You do not have the required authorization.']);
    }
    public function test_show()
    {
        $this->withExceptionHandling();
        $admin = Role::create(['name' => 'admin']);
        Permission::create(['name' => 'view user'])->assignRole($admin);
        $user = User::factory()->create(['name' => 'Luis', 'email' => 'Luis@gmail.com', 'password' => bcrypt('123456'), 'hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);
        $user->assignRole('admin');
        $this->actingAs($user)->withSession(['banned' => false])->getJson("api/v1/users/$user->id")
            ->assertStatus(200)
            ->assertJson(['data' => []]);
    }
    public function test_show_not_found()
    {
        $this->withExceptionHandling();
        $user = User::factory()->create(['name' => 'Luis', 'email' => 'Luis@gmail.com', 'password' => bcrypt('123456'), 'hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);
        $this->actingAs($user)->withSession(['banned' => false])->getJson("api/v1/users/100")
            ->assertStatus(404)
            ->assertJson(['message' => "Resource not found"]);
    }
    public function test_update()
    {
        $this->withExceptionHandling();
        $worker = Role::create(['name' => 'worker']);
        Permission::create(['name' => 'update user'])->assignRole($worker);
        $user = User::factory()->create(['name' => 'Luis', 'email' => 'Luis@gmail.com', 'password' => bcrypt('123456'), 'hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);
        $user->assignRole('worker');

        Region::factory()
            ->has(City::factory()->count(1)->has(Hotel::factory()->count(1)))
            ->create();

        Position::factory()->create(['name' => 'almacenero']);
        Strength::factory()->create(['name' => 'fuerza']);

        $payload = [
            'name' => 'luis',
            'lastname' => 'murrugarra',
            'gender' => 'masculino',
            'hotel' => [
                "id" => 1
            ],
            'position' => [
                "id" => 1
            ],
            'strength' => [
                "id" => 1
            ],
            'experience' => '100'
        ];
        $this->actingAs($user)->withSession(['banned' => false])->putJson("api/v1/users/$user->id", $payload)
            ->assertStatus(200)
            ->assertJson(['data' => [], 'message' => 'User Updated']);
    }
    public function test_update_comprimise()
    {
        $this->withExceptionHandling();
        $worker = Role::create(['name' => 'worker']);
        Permission::create(['name' => 'update user compromise'])->assignRole($worker);
        $user = User::factory()->create(['name' => 'Luis', 'email' => 'Luis@gmail.com', 'password' => bcrypt('123456'), 'hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);
        $user->assignRole('worker');
        $this->actingAs($user)->withSession(['banned' => false])->patchJson("api/v1/users/$user->id/compromise", [
            'compromise' => 'Yo Me comprometo a ...',
        ])
            ->assertStatus(200)
            ->assertJson(['data' => []]);
    }
    public function test_generate_secret_password()
    {
        $this->withExceptionHandling();
        $role = Role::create(['name' => 'worker']);
        $role = Role::create(['name' => 'admin']);
        Permission::create(['name' => 'create secret password'])->assignRole($role);
        $user = User::factory()->create(['name' => 'Luis', 'email' => 'Luis@gmail.com', 'password' => bcrypt('123456'), 'hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);
        $user->assignRole('admin');
        $payload = [
            'count' => 3
        ];
        $this->actingAs($user)->withSession(['banned' => false])->postJson("api/v1/users/secret-password", $payload)
            ->assertStatus(200)
            ->assertJson(['data' => []]);
    }
    public function test_show_my_suitcase()
    {
        $this->withExceptionHandling();
        $admin = Role::create(['name' => 'worker']);
        Permission::create(['name' => 'view suitcase'])->assignRole($admin);
        $user = User::factory()->create(['name' => 'Luis', 'email' => 'Luis@gmail.com', 'password' => bcrypt('123456'), 'hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);
        $user->assignRole('worker');
        $user->suitcase()->create(['name' => 'my suitcase']);
        $this->actingAs($user)->withSession(['banned' => false])->getJson("api/v1/users/$user->id/suitcase")
            ->assertStatus(200)
            ->assertJson(['data' => []]);
    }
    public function test_update_my_suitcase()
    {
        $this->withExceptionHandling();
        $worker = Role::create(['name' => 'worker']);
        Permission::create(['name' => 'update suitcase'])->assignRole($worker);
        $user = User::factory()->create(['name' => 'Luis', 'email' => 'Luis@gmail.com', 'password' => bcrypt('123456'), 'hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);
        $user->assignRole('worker');
        $category = Category::factory()->create();
        Thing::factory()
            ->count(5)
            ->for($category)
            ->create();
        // $user->suitcase()->create(['name' => 'my suitcase']);

        $payload = [
            'name' => 'Suitcase',
            'things' => [
                ["id" => 1],
                ["id" => 2]
            ],
        ];
        $this->actingAs($user)->withSession(['banned' => false])->putJson("api/v1/users/$user->id/suitcase", $payload)
            ->assertStatus(200)
            ->assertJson(['data' => [], 'message' => 'Suitcase Updated']);
    }
    public function test_index_workers()
    {
        $this->withExceptionHandling();
        $admin = Role::create(['name' => 'worker']);
        Permission::create(['name' => 'view workers'])->assignRole($admin);
        $user = User::factory()->create(['name' => 'Luis', 'email' => 'Luis@gmail.com', 'password' => bcrypt('123456'), 'hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);
        $user->assignRole('worker');
        $this->actingAs($user)->withSession(['banned' => false])->getJson("api/v1/workers")
            ->assertStatus(200)
            ->assertJson(['data' => []]);
    }
    public function test_show_worker()
    {
        $this->withExceptionHandling();
        $worker = Role::create(['name' => 'worker']);
        Permission::create(['name' => 'view worker'])->assignRole($worker);
        $user = User::factory()->create(['name' => 'Luis', 'email' => 'Luis@gmail.com', 'password' => bcrypt('123456'), 'hotel_id' => null, 'position_id' => null, 'strength_id' => null,]);
        $user->assignRole('worker');
        $this->actingAs($user)->withSession(['banned' => false])->getJson("api/v1/workers/$user->id")
            ->assertStatus(200)
            ->assertJson(['data' => []]);
    }
}
